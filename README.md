Want to keep track of your VyOS/Vyatta Firewall rules?
Besides that, it would be nice if there was a way to automatically 
create the statements for the rules?

If so: this project is the way to go for you.

In this project you'll find a Excel-Sheet with templates for firewall groups and
firewall rules. (Without the macro code for security reasons)

Besides that, you'll also find a textfile containing the code for the macros
and the form to present the configuration commands.

Combine these - boom, bob's your uncle!

Screenshot of my german variant:

![](german-example.png)